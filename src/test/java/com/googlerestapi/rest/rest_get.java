package com.googlerestapi.rest;


import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;


class rest_get {
	
	@BeforeClass
	public void setBaseURI(){
		RestAssured.baseURI = "https://maps.googleapis.com";
	}
	
	@Test
	
	public void testStatusCodeRestAssured(){
		//Response res;
		given()
		.param("query","gurgaon")
		.param("key","AIzaSyD5qbsbCi6gdJz69AOBGYlCi4vWuVvnfjY")
		
		.when()
		.get("/maps/api/place/textsearch/json")
		
		.then ()
		//.assertThat().statusCode(201);   //To fail the script
		.assertThat().statusCode(200).body(                       
                "results[0].formatted_address", is("Haryana, India")  
            );
            				
	}
	
	@Test
	public void testJSONExtractRestAssured(){
		Response res=
		given()
		.param("query","gurgaon")
		.param("key","AIzaSyD5qbsbCi6gdJz69AOBGYlCi4vWuVvnfjY")
		
		.when()
		.get("/maps/api/place/textsearch/json")
		
		.then ()
		.contentType(ContentType.JSON)
	    .extract().response();
	    System.out.println (res.asString ());			
	}
	

}


//https://maps.googleapis.com/maps/api/place/textsearch/json?query=gurgaon&key=AIzaSyD5qbsbCi6gdJz69AOBGYlCi4vWuVvnfjY
//https://techbeacon.com/how-perform-api-testing-rest-assured
1. Check different response code by passing valid, invalid and incorrect data.
2. Check the response by passing invalid endpoint URI.
3. Check the response message for different parameters passed as request. 
4. Print the complete response body for all above scenarios in logs. 
5. Check the response by passing invalid method type
6. Check the response for invalid or no authorisation. 
7. Verify the headers if required.
8. Validate the schema
9. Verify the response time
10. Verify API security using HTTPS and HTTP.
11. Use hamcrest hassize() function to verify the response body numeric values
There are Hamcrest matchers for a large number of different checks, including equalTo() for equality, lessThan() and greaterThan() for comparison, 
hasItem() to check whether a collection contains a given element, and many more. 
12. Extract the response using the Groovy GPath Expressions. e.g "MRData.CircuitTable.Circuits.circuitId"
13. Verify response content type.
14. Verify the query and path parameters and use parameterization for different set of data.





